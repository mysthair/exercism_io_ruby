class SpaceAge
  attr_reader :period

  @@HEARTH_PERIOD = 31557600.0

  def initialize(period)
    @period = period
  end

  #Given an age in seconds...
  def on_earth
    # Earth: orbital period 365.25 Earth days, or 31557600 seconds
    period / @@HEARTH_PERIOD
  end

  def on_mercury
    #   - Mercury: orbital period 0.2408467 Earth years
    period / (0.2408467 * @@HEARTH_PERIOD)
  end

  def on_venus
    #- Venus: orbital period 0.61519726 Earth years
    period / (0.61519726 * @@HEARTH_PERIOD)
  end

  def on_mars
    #- Mars: orbital period 1.8808158 Earth years
    period / (1.8808158 * @@HEARTH_PERIOD)
  end

  def on_jupiter
    #- Jupiter: orbital period 11.862615 Earth years
    period / (11.862615 * @@HEARTH_PERIOD)
  end

  def on_saturn
    #- Saturn: orbital period 29.447498 Earth years
    period / (29.447498 * @@HEARTH_PERIOD)
  end

  def on_uranus
    #- Uranus: orbital period 84.016846 Earth years
    period / (84.016846 * @@HEARTH_PERIOD)
  end

  def on_neptune
    #- Neptune: orbital period 164.79132 Earth years
    period / (164.79132 * @@HEARTH_PERIOD)
  end
end