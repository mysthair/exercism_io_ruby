class Array

  def keep
    return self unless block_given?
    array = []
    i = 0
    while i < length
      if yield(self[i])
        array << self[i]
      end
      i += 1
    end
    array
  end

  def discard
    return self unless block_given?
    array = []
    i = 0
    while i < length
      unless yield(self[i])
        array << self[i]
      end
      i += 1
    end
    array
  end
end

if __FILE__ == $0

  #  assert_equal [1, 3], [1, 2, 3].keep(&:odd?)
  puts "[1, 3] == [1, 2, 3].keep(&:odd?)"
  puts "[1, 3] == #{[1, 2, 3].keep(&:odd?)}"
  puts "#{[1, 3] == [1, 2, 3].keep(&:odd?)}"

  # assert_equal [1, 3, 5], [1, 2, 3, 4, 5].discard(&:even?)
  puts  "[1, 3, 5] == [1, 2, 3, 4, 5].discard(&:even?)"
  puts  "[1, 3, 5] == #{[1, 2, 3, 4, 5].discard(&:even?)}"
  puts  "#{[1, 3, 5] == [1, 2, 3, 4, 5].discard(&:even?)}"
end