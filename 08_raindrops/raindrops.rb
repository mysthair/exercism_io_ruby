module Raindrops

  def self.convert(n)
    rain = SOUNDS.map { |digit, sound| sound if n % digit == 0 }.join
    rain.empty? ? n.to_s : rain
  end

  SOUNDS = {3 => 'Pling', 5 => 'Plang', 7 => 'Plong'}
  private_constant :SOUNDS
  
end