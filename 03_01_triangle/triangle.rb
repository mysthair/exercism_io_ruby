class Triangle
  
  attr_reader :cote

  def initialize(cote)
    @cote = cote
  end

  def equilateral?
    cote[0] > 0 and cote.uniq.length == 1
  end

  def isosceles?
    cote.min > 0 \
      and cote.max < cote.min(2).sum \
      and cote.uniq.length <= 2
  end

  def scalene?
    cote.min > 0 \
      and cote.max < cote.min(2).sum \
      and !self.isosceles?
  end
end