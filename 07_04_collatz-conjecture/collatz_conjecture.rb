module CollatzConjecture
  def self.steps(n)
    raise ArgumentError if n <= 0
    count = 0
    until n==1
      n, count = n.even? ? n / 2 : n = 3 * n + 1, count +1
    end
    count
  end
end