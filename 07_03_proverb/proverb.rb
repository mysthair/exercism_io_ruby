class Proverb

  attr_reader :desires, :qualifier
  def initialize(*desires, **optionnal)
    @desires = desires
    @qualifier = optionnal[:qualifier]
  end

  def to_s
    desires.each_cons(2).map { |wanted, lost| 
      "For want of a #{wanted} the #{lost} was lost." 
    }.push("And all for the want of a #{qualifier.nil? ? desires.first : qualifier + ' ' + desires.first}.").join("\n")
  end
end