class Sieve
  attr_reader :max
  def initialize(max)
    @max = max
  end

  def primes
    (2..max).select { |p| (2...p).all? { |k| (p % k) != 0 } }
  end
end