module PhoneNumber

  def self.clean(phone)

    phone.sub!(
        /
          \A          # beginning of string
          (?:\+?1)?      # optionnal '+1' or '1', not captured
          \s*\({0,1}     # optionnal spaces, and one optionnal '('
          ([2-9]\d\d) # 3 digits (area code) who cannot starts with 0 or 1
          \)?[\s\.]*     # one optionnal ')', and optionnal spaces or dots
          ([2-9]\d\d) # 3 digits (exchange code) who cannot starts with 0 or 1
          [\s\-\.]*      # optionnal spaces, minus or dot
          (\d{4})     # 4 digits
          \s*            # optionnal spaces
          \Z          # end of string
        /x,           # free-spacing mode and comment regexp ;)
        '\1\2\3')     # replace by the captured motif \1\2\3, or nil 
    
  end
end