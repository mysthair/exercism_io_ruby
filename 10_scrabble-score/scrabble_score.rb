class Scrabble

  attr_reader :word

  def initialize(word)
    @word = word
  end
  
  def score
    return 0 if word.nil?
    word.upcase.each_char.sum(&LETTER_SCORES)
  end

  def self.score(word)
    self.new(word).score
  end

  LETTER_SCORES = %w{AEILNORSTU DG BCMP FHVWY K JX QZ} \
    .zip([1,2,3,4,5,8,10]) \
    .flat_map {|letters, score| letters.chars.product([score]) } \
    .to_h.tap {|h| h.default = 0 }
  
  private_constant :LETTER_SCORES

end