module Transpose

  def self.transpose(str_matrix)
    original = str_matrix.split("\n").map(&:chars)

    destination = []

    original.each_index do |i|
      original[i].each_index do |j|
        destination[j] ||= []
        destination[j][i] = original[i][j]
        (0..i-1).each { |k| destination[j][k] ||= ' ' }
      end
    end

    destination.map(&:join).join("\n")
  end
end