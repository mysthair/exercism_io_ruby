class Tournament
  FORMAT = "%-30s |%3s |%3s |%3s |%3s |%3s\n"
  TITLES = %w"Team MP W D L P"
  INVERT_RESULT = { 'win' => 'loss', 'draw' => 'draw', 'loss' => 'win' }
  private_constant :FORMAT, :TITLES, :INVERT_RESULT

  def self.tally(input)
    input.split("\n")
      .each_with_object(ad_hoc_hash, &method(:score_add)).values
      .sort_by(&:sort_order)
      .unshift(TITLES)
      .map(&method(:format_line))
      .join
  end

  private
  def self.ad_hoc_hash
    Hash.new { |teams, name| teams[name] = Team.new(name) }
  end

  def self.score_add(line, teams)
    host, guest, result = *line.split(';')
    teams[host].add_score(result)
    teams[guest].add_score(INVERT_RESULT[result])
  end

  def self.format_line(team)
    FORMAT % team.to_a
  end
end

class Team
  attr_reader :name, :won, :draw, :lost
  def initialize(team)
    @name, @won, @draw, @lost = team, 0, 0, 0
  end
  def add_score(result)
    @won += 1 if result == 'win'
    @draw += 1 if result == 'draw'
    @lost += 1 if result == 'loss'
  end
  def sort_order
    [-points, name]
  end
  def to_a
    [name, match_played, won, draw, lost, points]
  end
  def match_played
    won + draw + lost
  end
  def points
    3 * won + draw
  end
end