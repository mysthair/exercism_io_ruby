module Grains

  def self.square(n)

    raise ArgumentError, "case number must be  between 1 and 64" if n < 1 or 64 < n

    2 ** (n-1)

  end

  def self.total

    2 ** 64 - 1
    
  end
end