module RunLengthEncoding

  def self.encode(txt)
    txt.gsub(/(.)\1+/) { "#{$&.size}#{$1}" }
  end

  def self.decode(txt)
    txt.gsub(/(\d+)([\w\s])/) { "#{$2 * $1.to_i}" }
  end
end