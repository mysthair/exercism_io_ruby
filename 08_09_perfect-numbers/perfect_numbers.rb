module PerfectNumber
  ANSWERS = %w~perfect abundant deficient~
  def self.classify(n)
    raise 'must be positive' if n < 0
    ANSWERS[(1...n).sum { |i| (n % i).zero? ? i : 0 } <=> n]
  end
end