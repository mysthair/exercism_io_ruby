class Clock

  HOURS_IN_DAY, MINUTES_IN_HOUR = 24, 60
  private_constant :HOURS_IN_DAY, :MINUTES_IN_HOUR

  def initialize(hour: 0, minute: 0)
    @time = (MINUTES_IN_HOUR * hour + minute) % (MINUTES_IN_HOUR * HOURS_IN_DAY)
  end

  def hour_hand
    (time / MINUTES_IN_HOUR) % HOURS_IN_DAY
  end

  def minute_hand
    time % MINUTES_IN_HOUR
  end

  def to_s
    "%02d:%02d" % [hour_hand, minute_hand]
  end

  def +(other)
    Clock.new(minute:time + other.time)
  end

  def -(other)
    Clock.new(minute:time - other.time)
  end

  def ==(other)
    time == other.time
  end

  protected
  attr_reader :time
  def time=(minutes)
    @time = minutes % MINUTES_INHOUR * HOURS_IN_DAY
  end
end