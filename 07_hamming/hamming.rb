module Hamming

  def self.compute(dna1, dna2)

    raise ArgumentError if dna1.length != dna2.length
    
    dna1.each_char.zip(dna2.each_char).count { |a, b| a != b }

  end

end