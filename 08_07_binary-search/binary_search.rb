class BinarySearch

  attr_reader :list

  def initialize(list)
    raise ArgumentError, 'Unsorted List' unless list.each_cons(2).all? { |a, b| a <= b }

    @list = list
  end

  def search_for(item, left = 0, right = list.length - 1)
    mid = middle(left, right)

    return mid if item == list[mid]

    raise 'not present in list' if left == right

    return search_for(item, left, mid) if item < list[mid]

    search_for(item, mid + 1, right)
  end

  def middle(left = 0, right = list.length - 1)
    left + ((right - left) / 2).to_i
  end
end
