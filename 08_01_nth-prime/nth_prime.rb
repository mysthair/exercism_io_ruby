module Prime
  def self.nth(number)
    raise ArgumentError.new("Must be positive.") if number <= 0
    (3..).step(2).each_with_object( [2] ) { |integer, primes|
      return primes.last if primes.length == number
      primes << integer if primes.all?{ |prime| integer % prime != 0 }
    }
  end
end

## '(1..)' is valid in ruby >= 2.6   https://ruby-doc.org/core-2.6.1/Range.html
## '(1..)' is invalid in ruby-2.5.5  https://ruby-doc.org/core-2.5.5/Range.html
