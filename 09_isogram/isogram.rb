module Isogram
  def self.isogram?(str)
    uniq_letter_array?(str.downcase.scan(/[a-z]/))
  end

  private
  def self.uniq_letter_array?(array_of_letter)
    array_of_letter.length == array_of_letter.uniq.length
  end
end