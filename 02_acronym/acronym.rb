module Acronym
  def self.abbreviate(sequence)
    sequence.scan(/\b\w/).join.upcase
  end
end