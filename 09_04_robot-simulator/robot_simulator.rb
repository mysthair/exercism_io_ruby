class Simulator
  INSTRUCTIONS = { 'L' => :turn_left, 'R' => :turn_right, 'A' => :advance }

  def instructions(codes)
    codes.chars.map{ |c| INSTRUCTIONS[c] }
  end

  def place(robot, x:0, y:0, direction: :east)
    robot.at(x, y)
    robot.orient(direction)
  end

  def evaluate(robot, codes)
    instructions(codes).each { |instruction| robot.send instruction }
  end
end

class Robot
  DIRECTIONS = [:north, :east, :south, :west]
  MOVES = { north: [0, 1], east: [1, 0], south: [0, -1], west: [-1, 0] }
  private_constant :DIRECTIONS, :MOVES
  
  attr_reader :bearing, :coordinates

  def initialize(direction: :north, x: 0, y: 0)
    orient(direction)
    at(x, y)
  end

  def orient(direction)
    raise ArgumentError, "direction isn't in #{DIRECTIONS.inspect}" unless DIRECTIONS.include? direction
    @bearing = direction
  end

  def turn_right
    @bearing = DIRECTIONS[(DIRECTIONS.index(bearing) + 1) % 4]
  end

  def turn_left
    @bearing = DIRECTIONS[(DIRECTIONS.index(bearing) - 1) % 4]
  end

  def at(*coord)
    raise ArgumentError, "coord isn't array" unless  coord.is_a?(Array)
    raise ArgumentError, "coord isn't array of length 2, but #{coord.length} a s length #{coord.length}..." unless coord.length==2
    raise ArgumentError, "first value in coord isn'nt an Integer" unless coord[0].is_a?(Integer) 
    raise ArgumentError, "second value in coord isn'nt an Integer" unless coord[1].is_a?(Integer)
    @coordinates = coord
  end

  def advance
    @coordinates = [coordinates[0] + MOVES[bearing][0], coordinates[1] + MOVES[bearing][1]]
  end
end