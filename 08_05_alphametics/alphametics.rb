class Alphametics

  def self.solve(puzzle)
    new(puzzle).solve
  end

  attr_reader :puzzle

  def initialize(puzzle)
    @puzzle = puzzle.freeze
  end

  def variables
    @variables ||= puzzle.scan(/[A-Z]/).uniq.sort.freeze
  end

  def solve
    (0..9).to_a
    .permutation(variables.length)
    .select { |perm| test(puzzle.tr(variables.join, perm.join)) }
    .each { |perm| return variables.zip(perm).to_h }
    {}
  end

  private

  def test(equality)
    equality !~ /\b0\d+/ && eval(equality)
  end
end