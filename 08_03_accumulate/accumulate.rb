class Array
  def accumulate
    return self unless block_given?
    array = []
    i = 0
    while i < length
      array << yield(self[i])
      i += 1
    end
    array
  end
end

if __FILE__ == $0
  result = [1, 2, 3].accumulate do |number|
    number * number
  end
  puts "  [1, 4, 9] == #{result}  is #{[1, 4, 9] == result}"
  puts "#{ [1, 4, 9] == result ? 'OK' : 'KO' }"
end 