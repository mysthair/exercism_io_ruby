module Brackets
  BRACKETS = { '{' => '}', '[' => ']', '(' => ')' }

  def self.paired?(str)
    str.chars.each_with_object([]) { |c, required|
      if BRACKETS.keys.include? c
        required.push BRACKETS[c]
      elsif BRACKETS.values.include? c
        return false if required.empty? or ! c.eql? required.pop
      end
    }.empty?
  end
end