
class Palindromes
  
  attr_reader :min_factor, :max_factor, :largest, :smallest

  Result = Struct.new :value, :factors

  def initialize(min_factor: 1, max_factor: 1)
    @min_factor, @max_factor = min_factor, max_factor
  end

  def generate
    @smallest = Result.new(max_factor*max_factor, [])
    @largest = Result.new(min_factor*min_factor, [])

    (min_factor..max_factor).each do |a| 
      (a..max_factor).each do |b| 
        if (smallest.value >= (product = a*b) || largest.value <= product) \
          and self.class.palindromic?(product.to_s)
          smallest.value, @smallest.factors = product, [] if smallest.value > product
          largest.value, @largest.factors = product, [] if largest.value < product
          smallest.factors << [a,b] if smallest.value == product
          largest.factors << [a,b] if largest.value == product
        end
      end
    end
  end

  private
  def self.palindromic?(str)
    str == str.reverse
  end
end