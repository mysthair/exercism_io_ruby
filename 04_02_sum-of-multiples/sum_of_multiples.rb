class SumOfMultiples

  def initialize(*factor_list)
    @factor_list = factor_list
  end

  def to(n)
    # (1...n).sum { |i| factor_list.any? { |p| i % p == 0 } ? i : 0 }

    (1...n).select { |i| factor_list.any? { |p| i % p == 0 } }.sum

    #s = 0
    #(1...n).each { |i| s += i if factor_list.any? { |p| i % p == 0 } }
    #s

    #FAULT! (1...n).each_with_object(0) { |i, s| s += i if factor_list.any? { |p| i % p == 0 } }
    #OK (1...n).each_with_object( [0] ) { |i, s| s[0] += i if factor_list.any? { |p| i % p == 0 } }[0]    
  end

  private
  attr_reader :factor_list
end

if __FILE__ == $0
  sum_of_multiples = SumOfMultiples.new(2, 3, 5, 7, 11)
  puts "sum_of_multiples.to(10_000) = #{sum_of_multiples.to(10_000)}"
end