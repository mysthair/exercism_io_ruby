class Triplet

  attr_reader :x, :y, :z

  def initialize(x, y, z)
    @x, @y, @z = x, y, z
  end

  def sum
    x + y + z
  end

  def product
    x * y * z
  end

  def pythagorean?
    x**2 + y**2 == z**2
  end

  def to_s
    "[#{x}, #{y}, #{z}]"
  end

  def to_a
    [x, y , z]
  end

  def self.where(args)
    min_factor = args[:min_factor] || 1
    max_factor = args[:max_factor] || 425  # erf

    return where_sum(args[:sum]) if args[:sum]

    [*1..max_factor - 2].combination(2).each_with_object([]) do |comb, triplets|
      x, y = *comb
      next if (z = sqrt_int(x**2 + y**2)).nil?
      (min_factor / x...max_factor / x).each do |factor|
        if min_factor <= (x * factor) && (z * factor) <= max_factor
          triplets << Triplet.new(x * factor, y * factor, z * factor)
        end
      end
    end.uniq(&:product)
  end

  private
  def self.sqrt_int(n_pow_2)
    n = (n_pow_2**0.5).to_i
    n * n == n_pow_2 ? n : nil
  end
  # just a very speed solution ...
  def self.where_sum(fixed_sum)
    return [] if fixed_sum.odd?

    # there is only one loops with an iterator.
    # the complexity is O(n) i.e. proportionnaly with n
    (2...fixed_sum / 3).step(2).each_with_object([]) do |a, triplets|
      s, p = (fixed_sum - 3 * a) / 2, a**2 / 2
      delta = s**2 - 4 * p
      next unless delta >= 0
      delta_sqrt = (delta**0.5).to_i
      next unless delta_sqrt**2 == delta && (s + delta_sqrt).even?
      b, c = (s - delta_sqrt) / 2, (s + delta_sqrt) / 2
      x, y, z = a + b, a + c, a + b + c
      triplets << Triplet.new(x, y, z)
    end.uniq(&:to_a)
  end
end

# trinomial method : https://en.wikipedia.org/wiki/Quadratic_equation
# if s = a+b and p = a*b 
#   then a and b are solutions of X²-s*X+p=0      (1)
# but X² -s*X + p = 0 had 2 solutions : 
# (s - sqrt(delta))/2 and (s + sqrt(delta))/2     (2)
#     with delta = s²-4*p

# Dickson's method : https://en.wikipedia.org/wiki/Formulas_for_generating_Pythagorean_triples#Dickson's_method
# if we choose a,b,c such that a*a = 2*b*c                (3)
# then let x=a+b, y=a+c, z=a+b+c verify x*x + y*y = z*z   (4)
# let's prove it :
# x*x + y*y = (a+b)*(a+b) + (a+c)*(a+c)
#           = a*a + 2*a*b + b*b + a*a + 2*a*c + c*c
#           = a*a + 2*a*b + b*b + 2*b*c + 2*a*c + c*c  # because a*a = 2*b*c
#           = a*a + b*b + c*c + 2*a*b + 2*b*c + 2*a*c
#           = (a+b+c)*(a+b+c)
#           = z*z

# lets try to find a,b,c and x=a+b, y=a+c, z=a+b+c
# such than a*a = 2*b*c and x+y+z = fixed_sum

# BUT:
#     x+y+z = fixed_sum 
# <=> a+b + a+c + a+b+c = fixed_sum
# <=> 3*a + 2*b + 2*c = fixed_sum
# <=> b+c = (fixed_sum - 3*a)/2
# i.e. s = (fixed_sum - 3*a)/2                    (5)

# BUT: b*c = a*a/2
# i.e. p = a*a/2                                  (6)

# let's choose arbitrary a,
# and calculate b and c with (1)(2) and (5)(6)
# then calculate x,y,z with (4)
# then we obtain x,y,z who verify x+y+z = fixed_sum and x*x + y*y = z*z

if __FILE__ == $0
  sum = ARGV[0].to_i
  if sum > 0
    puts "Triplet.where(sum: #{sum}, max_factor:#{sum})"
    puts "#{Triplet.where(sum: sum, max_factor:sum).map(&:to_s).join("\n")}"
  else
    puts "usage : $0 <sum>\n\nwhere <sum> is a number: the sum of the triplet componant."
  end
end


# ✗ time ruby pythagorean_triplet.rb 30_000
# Triplet.where(sum: 30000, max_factor:30000)
# [1200, 14375, 14425]
# [1875, 14000, 14125]
# [5000, 12000, 13000]
# [6000, 11250, 12750]
# [7500, 10000, 12500]
# ruby pythagorean_triplet.rb 30_000  0,21s user 0,03s system 96% cpu 0,245 total


# ✗ time ruby pythagorean_triplet.rb 30_000_000
# Triplet.where(sum: 30000000, max_factor:30000000)
# [703125, 14640000, 14656875]
# [1200000, 14375000, 14425000]
# [1875000, 14000000, 14125000]
# [4400000, 12421875, 13178125]
# [5000000, 12000000, 13000000]
# [6000000, 11250000, 12750000]
# [6562500, 10800000, 12637500]
# [6960000, 10468750, 12571250]
# [7500000, 10000000, 12500000]
# ruby pythagorean_triplet.rb 30_000_000  2,23s user 0,02s system 99% cpu 2,250 total


# ✗ time ruby pythagorean_triplet.rb 100_000_000
# Triplet.where(sum: 100000000, max_factor:100000000)
# [2343750, 48800000, 48856250]
# [20000000, 37500000, 42500000]
# [21875000, 36000000, 42125000]
# ruby pythagorean_triplet.rb 100_000_000  5,99s user 0,02s system 99% cpu 6,021 total


# ruby pythagorean_triplet_test.rb
# Run options: --seed 8672
#
# # Running:
#
# ........
#
# Finished in 0.005686s, 1407.0838 runs/s, 1407.0838 assertions/s.
#
# 8 runs, 8 assertions, 0 failures, 0 errors, 0 skips

# Sorry if I look like I’m showing off.
# But I’ve been looking so hard that I’m still a little proud of myself.