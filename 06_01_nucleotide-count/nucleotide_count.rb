class Nucleotide

  def initialize(sequence)
    raise ArgumentError if sequence.scan(/[^ATCG]/).any? 
    @sequence = sequence
  end


  def self.from_dna(sequence)
    Nucleotide.new(sequence)
  end

  def count(char)
    sequence.scan(char).length
  end

  def histogram
    sequence.chars.each_with_object({'A'=>0, 'T'=>0, 'C'=>0, 'G'=>0}) { |char, h| h[char] += 1 }
  end

  private
  attr_reader :sequence

end