module Luhn
  
  def self.valid?(code)
    return false unless code.scan(/[^\d ]/).empty?

    digits = code.scan(/\d/).map(&:to_i)

    digits.size > 1 && checksum?(digits)
  end

  private 

  DOUBLE = [*0..9].map { |i| (i * 2 > 9) ? i * 2 - 9 : i * 2 }.freeze
  # DOUBLE = [0, 2, 4, 6, 8, 1, 3, 5, 7, 9].freeze

  def self.checksum?(digits)
    digits.reverse.
      each_slice(2).
      sum { |even, odd=0| even + DOUBLE[odd] }.
      modulo(10).
      zero?
  end
end