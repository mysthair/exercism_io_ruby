class Bucket
  attr_reader :size, :filled
  def initialize(size)
    @size, @filled = size, 0
  end
  def pouring_to(another)
    quantity = [filled, another.rest].min
    @filled -= quantity
    another.filled += quantity
  end
  def empty!
    @filled = 0
  end
  def fill!
    @filled = size
  end
  def rest
    size - filled
  end
  def empty?
    filled.zero?
  end
  def full?
    filled == size
  end
  protected
  attr_writer :filled
end

class TwoBucket

  NAMES = %w{one two}.freeze
  private_constant :NAMES

  attr_reader :first, :goal, :goal_bucket, :other_bucket, :moves

  def initialize(size_one, size_two, goal, first)
    @buckets = [Bucket.new(size_one), Bucket.new(size_two)]
    @first, @goal, @moves = first, goal, 0

    solve(NAMES.index(first))
  end

  private
  attr_reader :buckets

  def solve(first, second = 1 - first)
    until idx_goal = buckets.map(&:filled).index(goal)
      if moves.zero?
        buckets[first].fill!
      elsif buckets[second].size == goal
        buckets[second].fill!
      elsif buckets[first].empty?
        buckets[first].fill!
      elsif buckets[second].full?
        buckets[second].empty!
      else
        buckets[first].pouring_to(buckets[second])
      end
      @moves += 1
    end
    @goal_bucket = NAMES[idx_goal]
    @other_bucket = buckets[1 - idx_goal].filled
  end
end