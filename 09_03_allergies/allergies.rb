class Allergies
  ALLERGENS = %w{eggs peanuts shellfish strawberries tomatoes chocolate pollen cats}
  FLAGS = ALLERGENS.zip((0...ALLERGENS.length).map { |i| 1 << i }).to_h

  attr_reader :allergics_binary_list

  def initialize(allergics_binary_list)
    @allergics_binary_list = allergics_binary_list
  end

  def allergic_to?(allergen)
    allergics_binary_list & FLAGS[allergen] > 0
  end

  def list
    ALLERGENS.select(&method(:allergic_to?))
  end
end