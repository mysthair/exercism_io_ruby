class Anagram

  attr_reader :test_word

  def initialize(test_word)
    @test_word = test_word
  end

  def chars
    sorted_chars ||= test_word.downcase.chars.sort
  end

  def match(words_list)
    words_list.select { 
      |w| w.downcase.chars.sort == chars \
            and ! test_word.casecmp?(w)
    }
  end

  private
  attr_reader :sorted_chars
  
end