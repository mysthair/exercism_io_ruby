module Pangram
  def self.pangram?(txt)
    26 == txt.downcase.scan(/[a-z]/).uniq.length
  end
end