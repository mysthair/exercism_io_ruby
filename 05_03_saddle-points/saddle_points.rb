class Matrix
  
  attr_reader :str
  
  def initialize(str)
    @str = str
  end

  def rows
    str.each_line.map { |line| line.split.map(&:to_i) }
  end

  def columns
    rows.transpose
  end

  def saddle_points
    [*0...rows.length].product([*0...columns.length]).
      select { |l, c| saddle_point?(l, c) }
  end

  def saddle_point?(l, c)
    rows[l][c] == rows[l].max && rows[l][c] == columns[c].min
  end
end