module TwelveDays

  NTH = %w{first second third fourth fifth sixth seventh eighth ninth tenth eleventh twelfth}

  NUMBERS = %w{a two three four five six seven eight nine ten eleven twelve}

  GIFTS = [
    'Partridge in a Pear Tree', 'Turtle Doves',     'French Hens',
    'Calling Birds',            'Gold Rings',       'Geese-a-Laying',
    'Swans-a-Swimming',         'Maids-a-Milking',  'Ladies Dancing',
    'Lords-a-Leaping',          'Pipers Piping',    'Drummers Drumming'
  ]

  private_constant :NTH, :NUMBERS, :GIFTS

  def self.song
    (0..11).map { |i| lyric(i) }.join("\n\n")+ "\n"
  end

  def self.lyric(i)
    "On the #{NTH[i]} day of Christmas my true love gave to me: #{gifts(i)}."
  end

  private
  def self.gifts(i)
    to_phrase(*(i..0).step(-1).map { |j| "#{NUMBERS[j]} #{GIFTS[j]}" })
  end
  
  def self.to_phrase(*items)
    items.join(', ').gsub(/, (a[^\,]*)$/, ', and \1')
  end
end

if __FILE__ == $0
  puts "On the fourth day of Christmas my true love gave to me: four Calling Birds, three French Hens, two Turtle Doves, and a Partridge in a Pear Tree."
  puts "#{TwelveDays.lyric(3)}"
end