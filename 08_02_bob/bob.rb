module Bob
  def self.hey(remark)
    remark = remark.strip
    if remark.empty?
      "Fine. Be that way!"
    elsif remark.count('a-z') == 0 and remark.count('A-Z') > 0
      if remark.end_with?('?')
        "Calm down, I know what I'm doing!"
      else
        "Whoa, chill out!"
      end
    elsif remark.end_with?('?')
      "Sure."
    else
      "Whatever."
    end
  end
end