class Matrix

  attr_reader :rows

  def initialize(str)
    @rows = str.each_line.map { |line| line.split.map(&:to_i) }
  end

  def columns
    rows.transpose
  end
end