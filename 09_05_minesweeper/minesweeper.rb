class Board
  def self.transform(input)
    Board.new(input).transform
  end

  attr_reader :input

  def initialize(input)
    @input = input
    raise ArgumentError, 'different len' if input.any? { |line| line.size != input[0].size }
    raise ArgumentError, 'faulty border' unless border_test
    raise ArgumentError, 'invalid character' unless middle_test
  end

  def transform
    [*0...input.size].product([*0...input[0].size])
      .select { |l, c| bomb?(l, c) }
        .each{ |l, c| propagate_bomb(l , c) }
    input
  end

  private

  def border_test
    [0, l_max].product([0, c_max]).all? { |l, c| input[l][c] == '+' } and
    (1...c_max).all? { |c| input[0][c].eql? '-' and input[l_max][c].eql? '-' } and
    (1...l_max).all? { |l| input[l][0].eql? '|' and input[l][c_max].eql? '|' }
  end

  def middle_test
    [*1...l_max].product([*1...c_max]).all? { |l, c| input[l][c] =~ / |\*/ }
  end

  def c_max
    @c_max ||= input[0].size - 1
  end

  def l_max
    @l_max ||= input.size - 1
  end

  def bomb?(l, c)
    input[l][c] == '*'
  end
  
  def propagate_bomb(l , c)
    [*-1..1].product([*-1..1]).each {
      |x, y| counter_inc(l + x, c + y)
    }
  end

  def counter_inc(l, c)
    @input[l][c] = (input[l][c].to_i + 1).to_s if input[l][c] =~ /[ 1-7]/
  end
end