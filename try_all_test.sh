#!/bin/sh


echo "======$(date)=============================================================="
dirs=$(ls -d ??_*)
echo "dirs=$dirs"

echo "======$(date)=============================================================="
which tree && tree -R . || echo 'sorry tree not found'


echo "======$(date)=============================================================="
grep -e '^\s*skip' */*test.rb || true
grep -q -e '^\s*skip' */*test.rb && echo "error skip detected" && exit 1 || echo 'OK pas de skip en vue'
echo "======$(date)=============================================================="

ls -l
echo "======$(date)=============================================================="

echo "let's do the minitests"
for d in $dirs; do
    #if [ -d $d ]; then
        echo "###### $d ## $(date) #################"
        cd $d
        ruby *_test.rb || exit 2
        cd -
    #fi
done

echo "======$(date)=============================================================="
echo "all test done" 
echo "======$(date)=============================================================="

exit 0